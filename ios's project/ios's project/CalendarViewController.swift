//
//  calendarviewcontroller.swift
//  ios's project
//
//  Created by Guest User on 12/20/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController{
   
    @IBOutlet weak var calendarView: NWCalendarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarView.layer.borderWidth = 1
        calendarView.layer.borderColor = UIColor.lightGrayColor().CGColor
        calendarView.backgroundColor = UIColor.whiteColor()
        
        
        let date = NSDate()
        //    let newDate = date.dateByAddingTimeInterval(60*60*24*8)
        let newDate2 = date.dateByAddingTimeInterval(60*60*24*9)
        let newDate3 = date.dateByAddingTimeInterval(60*60*24*29)
        //    calendarView.disabledDates = [newDate, newDate2, newDate3]
        //    calendarView.availableDates = [newDate, newDate2, newDate3]
        calendarView.selectedDates = [newDate3, newDate2]
        calendarView.selectionRangeLength = 1
        calendarView.maxMonths = 4
        calendarView.delegate = calendarView.delegate
        calendarView.createCalendar()
        
        calendarView.scrollToDate(newDate3, animated: true)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

extension ViewController: NWCalendarViewDelegate {
    func didChangeFromMonthToMonth(fromMonth: NSDateComponents, toMonth: NSDateComponents) {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        let months = dateFormatter.standaloneMonthSymbols
        let fromMonthName = months[fromMonth.month-1] as String
        let toMonthName = months[toMonth.month-1] as String
        
        print("Change From '\(fromMonthName)' to '\(toMonthName)'")
    }
    
    func didSelectDate(fromDate: NSDateComponents, toDate: NSDateComponents) {
        print("Selected date '\(fromDate.month)/\(fromDate.day)/\(fromDate.year)' to date '\(toDate.month)/\(toDate.day)/\(toDate.year)'")
    }


}
