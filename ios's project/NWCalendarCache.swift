//
//  NWCalendarCache.swift
//  ios's project
//
//  Created by Guest User on 12/20/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation

class NWCalendarCache {
    static let sharedCache = NWCalendarCache()
    private var token: dispatch_once_t = 0
    
    var cache: NSCache!
    
    
    init() {
        dispatch_once(&token, {
            self.cache = NSCache()
        })
    }
    
    func objectForKey(key: AnyObject) -> AnyObject? {
        return cache.objectForKey(key)
    }
    
    func setObjectForKey(object: AnyObject, key: AnyObject) {
        cache.setObject(object, forKey: key)
    }
    
}