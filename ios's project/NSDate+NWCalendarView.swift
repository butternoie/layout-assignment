//
//  NSDate+NWCalendarView.swift
//  ios's project
//
//  Created by Guest User on 12/20/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//


import Foundation
import UIKit

extension NSDate {
    func nwCalendarView_dayWithCalendar(calendar: NSCalendar) -> NSDateComponents {
        return calendar.components([.Year, .Month, .Day, .Weekday, .Calendar], fromDate: self)
    }
    
    func nwCalendarView_monthWithCalendar(calendar: NSCalendar) -> NSDateComponents {
        return calendar.components([.Calendar, .Year, .Month], fromDate: self)
    }
    
    func nwCalendarView_dayIsInPast() -> Bool {
        return self.timeIntervalSinceNow <= NSTimeInterval(-86400)
    }
    
}