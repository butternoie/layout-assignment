//
//  NSCalendar+NWCalendarView.swift
//  ios's project
//
//  Created by Guest User on 12/20/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//'
import Foundation
import UIKit

extension NSCalendar {
    class func usLocaleCurrentCalendar() -> NSCalendar {
        let us = NSLocale(localeIdentifier: "en_US")
        let calendar = NSCalendar.currentCalendar()
        calendar.locale = us
        return calendar
    }
}
