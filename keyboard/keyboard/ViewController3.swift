//
//  ViewController3.swift
//  keyboard
//
//  Created by iOS Dev on 11/24/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var toptextField: UITextField!
    @IBOutlet weak var slidingView: UIView!
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHide:", name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tabAction(sender: AnyObject) {
        self.view.endEditing(false)
    }
    @IBOutlet weak var topConstrain: NSLayoutConstraint!

    @IBOutlet weak var buttomConstrain: NSLayoutConstraint!
    
    func keyboardHide(n:NSNotification){
        //self.navigationItem.rightBarButtonItem = nil
        self.topConstrain.constant = 0
        self.buttomConstrain.constant = 0
        self.view.layoutIfNeeded()
    }
    
    func keyboardShow(n:NSNotification){
        //self.navigationItem.rightBarButtonItem = self.rightNavButton
        let d = n.userInfo!
        var r = (d[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        let navHeight:CGFloat = 88
        let keyHeight = r.size.height + navHeight
        
        for subView in self.slidingView.subviews{
            if subView.isFirstResponder(){
                if subView.isKindOfClass(UITextField){
                    let textfield = subView as! UITextField
                    let viewHeight = self.view.frame.height
                    let textFieldBottom = textfield.frame.height + textfield.frame.origin.y
                    let topkey = viewHeight - keyHeight
                    let y : CGFloat = textFieldBottom - topkey
                    if topkey <= textFieldBottom{
                        self.topConstrain.constant = -y
                        self.buttomConstrain.constant = y
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
