//
//  PageInfo.swift
//  exam1
//
//  Created by iOS Dev on 12/3/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation
protocol PageInfo{
    var pageIndex: Int { get set }

}
