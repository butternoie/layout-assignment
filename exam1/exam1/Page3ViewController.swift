//
//  Page3ViewController.swift
//  exam1
//
//  Created by iOS Dev on 12/3/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class Page3ViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaSelected = MediaSelected()
        
        let ImageType = kUTTypeImage as String
        //let MovieType = kUTTypeMovie as String
        
        if let type = info[UIImagePickerControllerMediaType]{
            switch (type as! String){
        case ImageType:
            mediaSelected.mediaType = MediaType.IMAGE
        mediaSelected.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        imageView.image = mediaSelected.image
        break;
        case MovieType:
            mediaSelected.mediaType = MediaType.MOVIE
        mediaSelected.fileURL = (info[UIImagePickerControllerMediaURL] as? NSURL)!
        break;
        default:break;
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}