//
//  File.swift
//  exam1
//
//  Created by iOS Dev on 12/3/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation

import UIKit

enum MediaType{
    case IMAGE
    case MOVIE
    case UNKNOWN
}

class MediaSelected {
    var image:UIImage!
    var fileURL:NSURL!
    var mediaType:MediaType = MediaType.UNKNOWN
    init(){}
}
